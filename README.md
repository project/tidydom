#### Introduction

This module enables embedding a tidyDOM accessibility report in the Drupal admin dashboard.

tidyDOM is a paid service that requires a monthly subscription.

#### Configuration

1. Enter the API key that you generated for this site at /admin/config/services/tidydom/settings.
2. Visit /admin/reports/tidydom to see data from the latest scans, see configured pages, and download PDF and CSV reports.

#### Version Compatibility
| tidyDOM version | Drupal core | PHP |
| ------ | ------ | ----- |
| 1.x |8.8+ | 7.2+

#### Maintainers

[/u/jtolj](https://www.drupal.org/u/jtolj)
