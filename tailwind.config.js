module.exports = {
  mode: 'jit',
  purge: [
    './admin/src/**/*.svelte'
  ],
  darkMode: false,
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
