import App from "./App.svelte";

(function (Drupal, drupalSettings) {
  "use strict";
  let app;
  Drupal.behaviors.tidydom = {
    attach: function (context, settings) {
      if (app) {
        return;
      }
      const target = document.getElementById("tidydom-app");
      if (target) {
        app = new App({
          target,
        });
      }
    },
  };
})(Drupal, drupalSettings);
