import axios from 'axios';
let drupalBase = drupalSettings.path.baseURL || '/';
axios.defaults.baseURL = location.origin + drupalBase + 'tidydom/json/';

export const getReport = () => {
    return axios.get('report');
}

export const getPages = (page = 1) => {
    return axios.get('pages', {params: {page}});
}
