<?php

namespace Drupal\tidydom\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\tidydom\Service\ApiClient;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure the API settings for this site.
 *
 * @internal
 */
class AdminConfigForm extends ConfigFormBase {

  /**
   * @var \Drupal\tidydom\Service\ApiClient
   */
  protected $client;

  /**
   * Class constructor.
   */
  public function __construct(ApiClient $client) {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(ApiClient::class)
    );
  }

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return 'tidydom_admin_config';
  }

  /**
   * @inheritdoc
   */
  protected function getEditableConfigNames() {
    return ['tidydom.settings'];
  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tidydom.settings');

    $existing_key = $config->get('api_key');

    if ($existing_key) {
      if ($this->validateApiKey($existing_key)) {
        $form['status'] = [
          '#markup' => '<strong>' . $this->t('API Key has been set and is valid.') . '</strong>',
        ];
      }
      else {
        $form['status'] = [
          '#markup' => '<strong>' . $this->t('Could not connect to tidyDOM with the current API key. Please check the API Key is correct.') . '</strong>',
        ];
      }
    }

    $form['api_key'] = [
      '#type' => [],
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t("The API Key for this site from tidyDOM.com"),
      '#required' => TRUE,
      '#attributes' => [],
    ];

    if ($existing_key) {
      $form['api_key']['#attributes']['placeholder'] = "***********************";
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * @inheritdoc
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (!$this->validateApiKey($form_state->getValue('api_key'))) {
      $form_state->setErrorByName('api_key', $this->t("Could not connect to tidyDOM with the API Key provided. Please check the key and try again."));
    }

  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('tidydom.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Test whether the provided API key is valid.
   *
   * @param string $api_key
   *   The API key to check.
   *
   * @return bool
   *   Whether a successful API connection was made.
   */
  protected function validateApiKey($api_key) {
    preg_match('/^(\w+)\|(.*)/', $api_key, $matches);
    $site_id = isset($matches[1]) ? $matches[1] : NULL;
    $api_key = isset($matches[2]) ? $matches[2] : NULL;
    return $this->client->ping([
      'headers' => [
        'Authorization' => "Bearer {$api_key}",
      ],
    ], $site_id);
  }

}
