<?php

namespace Drupal\tidydom\Controller;

use Drupal\tidydom\Service\ApiClient;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An example controller.
 */
class ApiController extends ControllerBase {

  /**
   * @var \Drupal\tidydom\Service\ApiClient
   */
  protected $client;

  /**
   * Class constructor.
   */
  public function __construct(ApiClient $client) {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(ApiClient::class)
    );
  }

  /**
   * Returns a structured object of scan data.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The structured report data.
   */
  public function report() {
    try {
      $response = new JsonResponse($this->client->scans());
    }
    catch (RequestException $e) {
      $response = new JsonResponse([], $e->getResponse()->getStatusCode());
    }
    catch (\Exception $e) {
      $response = new JsonResponse([], 500);
    }

    return $response;
  }

  /**
   * Returns a structured object of page data.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The structured report data.
   */
  public function pages() {
    try {
      $response = new JsonResponse($this->client->pages());
    }
    catch (RequestException $e) {
      $response = new JsonResponse([], $e->getResponse()->getStatusCode());
    }
    catch (\Exception $e) {
      $response = new JsonResponse([], 500);
    }

    return $response;
  }

  /**
   *
   * @param string $type
   *   The report type - 'csv' or 'pdf'.
   *
   * @return \Symfony\Component\HttpFoundation\StreamedResponse
   *   A streamed response of the download.
   */
  public function download($type) {
    return $this->client->downloadReport($type);
  }

}
