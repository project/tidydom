<?php

namespace Drupal\tidydom\Controller;

use Drupal\Core\Config\Config;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class ReportController extends ControllerBase {

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Class constructor.
   */
  public function __construct(Config $config) {
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tidydom.settings')
    );
  }

  /**
   * Returns a render-able array for the accessibility report page.
   */
  public function show() {

    if ($this->config->get('api_key')) {
      $markup = '<div id="tidydom-app"></div>';
    }
    else {
      $markup = '<h2>No API Key Configured</h2>';
      $markup .= Link::createFromRoute('Configure your API Key', 'tidydom.settings.form')->toString();
    }
    return [
      '#markup' => $markup,
      '#attached' => [
        'library' => [
          'tidydom/report_app',
        ],
      ],
    ];

  }

}
