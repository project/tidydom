<?php

namespace Drupal\tidydom\Service;

use Drupal\Core\Config\Config;
use Drupal\Core\Http\ClientFactory;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 *
 */
class ApiClient {


  /**
   *
   * @var \GuzzleHttp\Client
   */
  protected $http;

  /**
   *
   * @var string
   */
  protected $baseUrl = 'https://api.tidydom.com';

  /**
   *
   * @var string
   */
  protected $apiVersion = 'v1';

  /**
   *
   * @var string
   */
  protected $siteId;

  /**
   * ApiClient constructor.
   */
  public function __construct(Config $config, ClientFactory $http_client_factory) {

    $api_config = $config->get('api_key');
    preg_match('/^(\w+)\|(.*)/', $api_config, $matches);
    $site_id = isset($matches[1]) ? $matches[1] : NULL;
    $api_key = isset($matches[2]) ? $matches[2] : NULL;

    $this->http = $http_client_factory->fromOptions([
      'headers' => [
        'Authorization' => "Bearer {$api_key}",
        'Accept' => 'application/json',
      ],
    ]);

    $this->siteId = $site_id;
  }

  /**
   * Helper function to build the API URL.
   *
   * @param string $endpoint
   *   The endpoint to call.
   *
   * @return string
   *   The full URL to the endpoint.
   */
  protected function getUrl($endpoint) {
    return "{$this->baseUrl}/{$this->apiVersion}/{$endpoint}";
  }

  /**
   * Test for a valid response.
   *
   * @param array $options
   *   Options to pass to \GuzzleHttp\Client.
   * @param string $site_id
   *   Optionally override the default site id.
   *
   * @return bool
   *   Whether a request returned a 200 response.
   */
  public function ping(array $options = [], $site_id = NULL) {
    $site_id = $site_id ?? $this->siteId;
    try {
      $request = $this->http->get($this->getUrl("site/{$site_id}/scans"), $options);
    }
    catch (RequestException $e) {

    }

    return isset($request) && $request->getStatusCode() === 200;
  }

  /**
   * Fetch scan data for this site.
   *
   * @return array
   *   A structured array of scan data to return to the front end as json.
   */
  public function scans() {
    $request = $this->http->get($this->getUrl("site/{$this->siteId}/scans"));

    $response = json_decode($request->getBody());

    $chart_data = [
      'labels' => [],
      'series' => [[]],
    ];
    $table_data = [
      'caption' => 'Results',
      'headers' => [
        'Date',
        'Score',
        '# Pages',
        'Errors',
        'Warnings',
        'Total Active Issues',
      ],
      'rows' => [],
    ];

    $num_rows = count($response->data);

    foreach ($response->data as $index => $scan) {
      $date = \DateTime::createFromFormat('Y-m-d', substr($scan->date, 0, 10))->format('m/d/y');
      $chart_data['labels'][] = $scan->human_date;
      $chart_data['series'][0][] = $scan->statistics->score;
      $table_data['rows'][] = [
        $date,
        $scan->statistics->score,
        $scan->statistics->total_pages,
        $scan->statistics->errors,
        $scan->statistics->warnings,
        $scan->statistics->active_issues,
      ];

      if ($num_rows === 1) {
        $start_date = $date;
      }
      if ($index === 0) {
        $end_date = $date;
      }
      if (!isset($start_date) && $index === $num_rows - 1) {
        $start_date = $date;
      }
    }
    if (!empty($start_date) && !empty($end_date)) {
      $table_data['caption'] .= " {$start_date} - {$end_date}";
    }

    $chart_data['labels']    = array_reverse($chart_data['labels']);
    $chart_data['series'][0] = array_reverse($chart_data['series'][0]);

    return [
      'site' => $response->meta->site->data[0],
      'scan' => $response->data[0] ?? NULL,
      'team'      => $response->meta->team->data[0],
      'chartData' => $chart_data,
      'tableData' => $table_data,
    ];
  }

  /**
   * Fetch page data for this site.
   *
   * @param int $page
   *   The paginator page to pass through to the API request.
   *
   * @return array
   *   A structured array of page data to return to the front end as json.
   */
  public function pages($page = 1) {
    $request = $this->http->get(
      $this->getUrl("site/{$this->siteId}/pages"),
      ['query' => ['page' => (int) $page]]
    );

    $response = json_decode($request->getBody());

    $table_data = [
      'caption' => 'Pages Scanned',
      'headers' => [
        'Path',
        '# Instances',
        'Scan Status',
      ],
      'rows'    => [],
    ];

    foreach ($response->data as $page) {
      $table_data['rows'][] = [
        $page->path,
        "{$page->warning_count} Warnings / {$page->error_count} Errors",
        $page->status ? 'OK' : $page->last_scan_error,
      ];
    }

    return [
      'site'       => $response->meta->site->data[0],
      'tableData'  => $table_data,
      'pagination' => $response->meta->pagination,
    ];

  }

  /**
   * Download PDF or CSV report.
   *
   * @param string $type
   *   The report type to download - 'csv' or 'pdf'.
   *
   * @return \Symfony\Component\HttpFoundation\StreamedResponse
   *   A streamed response with the file download.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function downloadReport($type) {

    if (!in_array($type, ['csv', 'pdf'])) {
      throw new NotFoundHttpException();
    }

    $url = $this->getUrl("site/{$this->siteId}/report") . '?' . http_build_query(['type' => $type]);

    try {
      $request = $this->http->get($url);
      $body = $request->getBody();

      $response = new StreamedResponse(function () use ($body) {
        while (!$body->eof()) {
          echo $body->getContents();
          ob_flush();
          flush();
        }
      }, 200, [
        "Content-Disposition" => "attachment; filename=\"accessibility-report.{$type}\"",
        'Pragma' => 'public',
        'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
      ]);

      return $response;
    }
    catch (RequestException $e) {
      $message = sprintf("Error downloading report: Code %d", $e->getResponse()->getStatusCode());
      return new Response($message, $e->getResponse()->getStatusCode());
    }
  }

}
